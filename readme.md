## Mozilla Thunderbird with the sequoia octopus openpgp library

This repo provides 3 different Thunderbird packages.
All are based on the default `thunderbird` package,
but `librnp.so` has been replaced by a version of the octopus:
- `thunderbird-octopus`: Uses a git checkout of the octopus, no local dependencies.
- `thunderbird-octopus-dev`: Uses my development build of the octopus.
You likely have do adjust the path to `debug/libsequoia_octopus_librnp.so` to suit your needs.
- `thunderbird-octopus-dev-release`: Same as above, but uses the release build.

### Usage
You can build the packages like this:
```
$ cd thunderbird-octopus
$ nix-build build.nix -A thunderbird-octopus-dev
$ ./result/bin/thunderbird
```
