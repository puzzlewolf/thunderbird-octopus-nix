self: super:
{
  thunderbird-octopus = super.callPackage ./pkgs/default.nix { };
  thunderbird-octopus-dev = super.callPackage ./pkgs/dev.nix { };
  thunderbird-octopus-dev-release = super.callPackage ./pkgs/dev-release.nix { };
}
