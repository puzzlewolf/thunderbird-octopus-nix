
{ pkgs, stdenv, thunderbird }:
let
  sequoia-octopus-librnp = pkgs.callPackage ./octopus.nix { };
in stdenv.mkDerivation {

  name = "thunderbird-octopus-dev";
  dontUnpack = true;
  dontPatch = true;
  dontConfigure = true;
  dontBuild = true;
  installPhase = ''
    cp -r ${thunderbird} $out
    substituteInPlace $out/bin/thunderbird \
      --replace ${thunderbird} $out
    # Coming from the nix store, this is not writable by default
    chmod +w $out/lib/thunderbird
    rm $out/lib/thunderbird/librnp.so
    ln -s /home/nora/Projects/sequoia-octopus-librnp/target/debug/libsequoia_octopus_librnp.so $out/lib/thunderbird/librnp.so
  '';

  # Links to my local development build. Very impure, but handy :)

  meta = with stdenv.lib; {
    description = "thunderbird using the sequoia pgp library, local debug build";
    homepage = "https://gitlab.com/sequoia-pgp/sequoia-octopus-librnp";
    license = licenses.gpl3;
    maintainers = with maintainers; [ puzzlewolf ];
    platforms = platforms.all;
  };
}

