{ stdenv, rustPlatform, fetchFromGitLab, llvmPackages_10, pkg-config, openssl
, sqlite, nettle, cargo, rustc, capnproto, ensureNewerSourcesForZipFilesHook }:

rustPlatform.buildRustPackage rec {
  pname = "sequoia-octopus-librnp";
  version = "0.0.0";

  #https://gitlab.com/sequoia-pgp/sequoia-octopus-librnp.git
  src = fetchFromGitLab {
    owner = "sequoia-pgp";
    repo = "sequoia-octopus-librnp";
    rev = "4e7e54aa432cac858e5ef11b6a22ffde4c311e0a";
    hash = "sha256:0000000000000000000000000000000000000000000000000000";
  };

  cargoSha256 = "sha256:0000000000000000000000000000000000000000000000000000";

  # Steps to updating the package:
  # 1. Set src.rev to the desired revision
  # 2. Force rebuilding both the source and vendored depdendency derivations.
  #    We don't know their new hashes yet, but we definitely want to rebuild instead of pulling
  #    the old derivations (known by their hash) from the store.
  #    src.hash = "sha256:0000000000000000000000000000000000000000000000000000";
  #    cargoSha256 = "sha256:0000000000000000000000000000000000000000000000000000";
  # 3. Try to build the package, it will fail with an error message like:
  #    hash mismatch in fixed-output derivation '/nix/store/m1ga09c0z1a6n7rj8ky3s31dpgalsn0n-source':
  #    wanted: sha256:0000000000000000000000000000000000000000000000000000
  #    got:    sha256:173gxk0ymiw94glyjzjizp8bv8g72gwkjhacigd1an09jshdrjb4
  #    Use the "got" hash as src.hash
  # 4. Repeat 3. for cargoSha256

  LIBCLANG_PATH = "${llvmPackages_10.libclang}/lib";

  nativeBuildInputs = [
    pkg-config
    cargo
    rustc
    llvmPackages_10.libclang
    llvmPackages_10.clang
    ensureNewerSourcesForZipFilesHook
    capnproto
  ];

  buildInputs = [ openssl sqlite nettle ];

  meta = with stdenv.lib; {
    description = "";
    homepage = "https://gitlab.com/sequoia-pgp/sequoia-octopus-librnp";
    license = licenses.gpl3;
    maintainers = with maintainers; [ puzzlewolf ];
    platforms = platforms.all;
  };
}
