
{ pkgs, stdenv, thunderbird }:
let
  sequoia-octopus-librnp = pkgs.callPackage ./octopus.nix { };
in stdenv.mkDerivation {

  name = "thunderbird-octopus";
  dontUnpack = true;
  dontPatch = true;
  dontConfigure = true;
  dontBuild = true;
  installPhase = ''
    cp -r ${thunderbird} $out
    # Coming from the nix store, this is not writable by default
    substituteInPlace $out/bin/thunderbird \
      --replace ${thunderbird} $out
    chmod +w $out/lib/thunderbird
    rm $out/lib/thunderbird/librnp.so
    cp ${sequoia-octopus-librnp}/lib/libsequoia_octopus_librnp.so $out/lib/thunderbird/librnp.so
  '';

  meta = with stdenv.lib; {
    description = "thunderbird using the sequoia pgp library";
    homepage = "https://gitlab.com/sequoia-pgp/sequoia-octopus-librnp";
    license = licenses.gpl3;
    maintainers = with maintainers; [ puzzlewolf ];
    platforms = platforms.all;
  };
}

